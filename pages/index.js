import React from "react";
import {
  Badge,
  Card,
  DisplayText,
  EmptyState,
  Heading,
  Layout,
  Page,
  SkeletonBodyText,
  SkeletonPage,
  Stack,
  TextContainer,
} from "@shopify/polaris";
import { TitleBar, useAppBridge } from "@shopify/app-bridge-react";
import { useMutation, useQuery } from "@apollo/react-hooks";
import gql from "graphql-tag";
import { HttpLink } from "apollo-link-http";
import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { Redirect } from "@shopify/app-bridge/actions";

const img = "https://f000.backblazeb2.com/file/centoll/illustration_empty.svg";

const Index = (props) => {
  const app = useAppBridge();
  const redirect = Redirect.create(app);

  const {
    loading: check_valid_account_loading,
    data: check_valid_account_data,
  } = useQuery(CHECK_VALID_STRIPE_CONNECT_ACCOUNT__QUERY, {
    client: apolloClient,
    variables: { shop: props.shopOrigin },
  });

  const [
    getNewConnectAccountOnboardingUrl,
    { loading: onboarding_url_loading },
  ] = useMutation(GET_NEW_CONNECT_ACCOUNT_ONBOARDING_URL__MUTATION, {
    client: apolloClient,
  });

  const [
    getStripeConnectAccountLoginLink,
    { loading: login_link_loading },
  ] = useMutation(GET_STRIPE_CONNECT_ACCOUNT_LOGIN_LINK__MUTATION, {
    client: apolloClient,
  });

  const onboardingHandler = () => {
    getNewConnectAccountOnboardingUrl({
      variables: { shop: props.shopOrigin },
    }).then((res) => {
      redirect.dispatch(
        Redirect.Action.REMOTE,
        res.data.getNewConnectAccountOnboardingUrl
      );
    });
  };

  const loginHandler = () => {
    getStripeConnectAccountLoginLink({
      variables: { shop: props.shopOrigin },
    }).then((res) => {
      window.open(res.data.getStripeConnectAccountLoginLink);
    });
  };

  return (
    <Page>
      <TitleBar title="Home" />
      <Layout>
        <Layout.Section>
          {check_valid_account_loading ? (
            <SkeletonPage>
              <Layout>
                <Layout.Section>
                  <Card sectioned>
                    <SkeletonBodyText />
                  </Card>
                </Layout.Section>
              </Layout>
            </SkeletonPage>
          ) : check_valid_account_data &&
            check_valid_account_data.checkValidStripeConnectAccount ? (
            <Layout>
              <Layout.Section>
                <DisplayText size="large">
                  You are now connected to the global cart
                </DisplayText>
              </Layout.Section>
              <Layout.AnnotatedSection
                title="Status"
                description="Wether your account is ready or an action is needed."
              >
                <Card
                  footerActionAlignment="left"
                  primaryFooterAction={{
                    content: "View dashboard",
                    onAction: () => loginHandler(),
                    loading: login_link_loading,
                  }}
                >
                  <Card.Header
                    title={
                      <Stack alignment="center">
                        <Stack.Item>
                          <Heading>Integration status</Heading>
                        </Stack.Item>
                        <Stack.Item>
                          <Badge status="success">Connected</Badge>
                        </Stack.Item>
                      </Stack>
                    }
                  ></Card.Header>
                  <Card.Section>
                    <TextContainer>
                      <p>
                        Your shop is ready to receive orders from Centoll. You
                        can now access your Centoll Dashboard for reviewing the
                        channel's activity.
                      </p>
                    </TextContainer>
                  </Card.Section>
                </Card>
              </Layout.AnnotatedSection>
            </Layout>
          ) : (
            <Card>
              <EmptyState
                heading="Get in the future of checkout"
                image={img}
                action={{
                  content: "Connect Centoll account",
                  onAction: () => onboardingHandler(),
                  loading: onboarding_url_loading,
                }}
              >
                <p>
                  Allow users to order your products along millions of other
                  merchants.
                </p>
              </EmptyState>
            </Card>
          )}
        </Layout.Section>
      </Layout>
    </Page>
  );
};

const CHECK_VALID_STRIPE_CONNECT_ACCOUNT__QUERY = gql`
  query($shop: String!) {
    checkValidStripeConnectAccount(shop: $shop)
  }
`;

const GET_NEW_CONNECT_ACCOUNT_ONBOARDING_URL__MUTATION = gql`
  mutation($shop: String!) {
    getNewConnectAccountOnboardingUrl(shop: $shop)
  }
`;

const GET_STRIPE_CONNECT_ACCOUNT_LOGIN_LINK__MUTATION = gql`
  mutation($shop: String!) {
    getStripeConnectAccountLoginLink(shop: $shop)
  }
`;

const { NEXT_PUBLIC_BACKEND_HTTP_LINK } = process.env;
const httpLink = new HttpLink({
  uri: NEXT_PUBLIC_BACKEND_HTTP_LINK,
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache({ addTypename: false }),
});

export default Index;
