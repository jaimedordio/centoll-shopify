# Centoll Shopify
Repository for the Centoll Shopify App

## For installing the App in a store
- [https://centoll-shopify.herokuapp.com/auth/shopify?shop=express-shop-developing.myshopify.com](https://centoll-shopify.herokuapp.com/auth/shopify?shop=express-shop-developing.myshopify.com)
- [https://centoll-shopify.herokuapp.com/auth/shopify?shop=plugindeveloping.myshopify.com](https://centoll-shopify.herokuapp.com/auth/shopify?shop=plugindeveloping.myshopify.com)
