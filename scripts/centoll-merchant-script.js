console.log("Centoll loaded 🚀 🛒");

let iframe = document.createElement("iframe");

iframe.id = "centoll-iframe";
iframe.style.display = "none";
iframe.src = "https://app.centoll.com/iframe";

document.body.appendChild(iframe);

window.addEventListener(
  "message",
  (e) => {
    if (e.origin.includes("localhost" || "centoll")) {
      console.log(`New message from ${e.origin}: `, e.data);
      localStorage.setItem("centoll_userId", e.data);
    }
  },
  false
);

const getCookie = (cname) => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return null;
};

const variables = {
  userId: localStorage.getItem("centoll_userId"),
  cartId: getCookie("cart"),
};

/**
 * Link cart with user
 */
if (variables.userId && variables.cartId) {
  fetch("https://api.centoll.com/graphql", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `
        mutation ($userId: String!, $cartId: String!) {
          linkUserAndCart(userId: $userId, cartId: $cartId)
        }
      `,
      variables,
    }),
  })
    .then((res) => res.json())
    .then((result) => console.log(result));
}
