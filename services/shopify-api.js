const { getShopAccessToken } = require("./centoll-api");

/**
 * Get Shopify shop properties.
 *
 * @param {string} shop - The shop domain.
 * @param {string} accessToken - Access token for shop.
 *
 * @returns {Object} All shop properties
 */
const getShopProperties = (shop, accessToken) => {
  if (shop && accessToken) {
    return fetch(`https://${shop}/admin/api/2021-01/shop.json`, {
      method: "GET",
      headers: {
        "X-Shopify-Access-Token": accessToken,
      },
    })
      .then((res) => res.json())
      .catch((err) => {
        throw new Error(`Error fetching shop properties: ${err}`);
      });
  } else {
    if (!shop) throw new Error(`shop not provided`);
    if (!accessToken) throw new Error(`accessToken not provided`);
  }
};

/**
 * Check Webhook endpoint for the app
 *
 * @param {string} shop - The shop domain.
 * @param {string} address - The wanted address to compare, if different, it will be changed.
 */
const checkWebhookEndpoint = async (shop, address) => {
  if (!shop) throw new Error(`shop not provided`);
  else {
    const accessToken = await getShopAccessToken(shop);

    const webhook = await fetch(
      `https://${shop}/admin/api/2021-01/webhooks.json`,
      {
        method: "GET",
        headers: {
          "X-Shopify-Access-Token": accessToken,
        },
      }
    )
      .then((res) => res.json())
      .then(({ webhooks }) => {
        if (webhooks) {
          return webhooks.find((webhook) => webhook.topic === "carts/update");
        }

        return null;
      })
      .catch((err) => {
        console.error("Error retrieving webhooks", err);
        return null;
      });

    if (webhook && webhook.address !== address) {
      await fetch(
        `https://${shop}/admin/api/2021-01/webhooks/${webhook.id}.json`,
        {
          method: "PUT",
          headers: {
            "X-Shopify-Access-Token": accessToken,
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            webhook: {
              id: webhook.id,
              address,
            },
          }),
        }
      )
        .then((res) => res.json())
        .then((webhookJson) =>
          console.log(`Updated ${webhookJson.topic} webhook: ${webhook}`)
        )
        .catch((err) => {
          console.error("Error updating webhook", err);
        });
    }
  }
};

/**
 * Register a new Script Tag.
 *
 * @param {string} shop - The shop domain.
 * @param {string} accessToken - Access token for shop.
 *
 * @returns {Boolean} Successfully registered
 */
const registerScriptTag = (shop, accessToken) => {
  if (shop && accessToken) {
    return fetch(`https://${shop}/admin/api/2021-01/script_tags.json`, {
      method: "POST",
      headers: {
        "X-Shopify-Access-Token": accessToken,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        script_tag: {
          event: "onload",
          src:
            "https://gitlab.com/jaimedordio/centoll-shopify/-/raw/master/scripts/centoll-merchant-script.js",
        },
      }),
    })
      .then((res) => res.json())
      .catch((err) => {
        throw new Error(`Error creating ScriptTag: ${err}`);
      });
  } else {
    if (!shop) throw new Error(`shop not provided`);
    if (!accessToken) throw new Error(`accessToken not provided`);
  }
};

module.exports = { getShopProperties, checkWebhookEndpoint, registerScriptTag };
