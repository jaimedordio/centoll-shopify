const { config } = require("dotenv");
const { createApolloFetch } = require("apollo-fetch");
const { fetchProductImage } = require("./shopify-api");
const chalk = require("chalk");

config();
const { NEXT_PUBLIC_BACKEND_HTTP_LINK } = process.env;

const apolloFetch = createApolloFetch({
  uri: NEXT_PUBLIC_BACKEND_HTTP_LINK,
});

/**
 * Save Shopify shop properties on DB.
 *
 * @param { Object } shop_properties - Shopify shop properties response (ShopInput!).
 *
 * @returns {Object} Saved properties
 */
const saveShopProperties = async (shop_properties) => {
  if (!shop_properties) throw new Error("shop_properties not provided");
  else {
    try {
      const query = `
        mutation ($shop: ShopInput!) {
          saveShopProperties(shop: $shop)
        }
    `;

      const variables = {
        shop: shop_properties,
      };

      const save_shop_properties_response = await apolloFetch({
        query,
        variables,
      });

      return save_shop_properties_response;
    } catch (error) {
      throw new Error(`Error saving shop properties: ${error}`);
    }
  }
};

/**
 * Get shop access token.
 *
 * @param {string} shop - Shop domain.
 *
 * @returns {string} Access token
 */
const getShopAccessToken = async (shop) => {
  if (!shop) throw new Error("shop not provided");
  else {
    try {
      const query = `
      query ($shop: String!) {
        getShopAccessToken(shop: $shop) 
      }
    `;

      const variables = {
        shop,
      };

      const response = await apolloFetch({
        query,
        variables,
      });

      return response.data.getShopAccessToken;
    } catch (error) {
      throw new Error(`Error getting access token: ${error}`);
    }
  }
};

/**
 * Send cart to Backend
 *
 * @param {Object} webhookInfo - Webhook from Shopify.
 * @param {string} accessToken - Access token for shop.
 * @param {string} shop - The shop domain.
 */
const sendCart = async (webhookInfo, accessToken, shop) => {
  if (!webhookInfo) throw new Error("webhookInfo not provided");
  if (!accessToken) throw new Error("accessToken not provided");
  if (!shop) throw new Error("shop not provided");
  else {
    try {
      const items = webhookInfo.line_items.map((item) => ({
        quantity: item.quantity,
        variantId: item.variant_id,
      }));

      const cart = {
        cartId: webhookInfo.id,
        shop,
        items,
        updated_at: webhookInfo.updated_at,
        created_at: webhookInfo.created_at,
      };

      const query = `mutation ($cart: CartInput!) {
        updateShopifyCart(cart: $cart)
      }`;

      const variables = {
        cart,
      };

      return await apolloFetch({ query, variables });
    } catch (error) {
      throw new Error(`Error sending cart: ${error}`);
    }
  }
};

/**
 * Check if shop has an active install.
 *
 * @param {string} shop - Shop domain.
 *
 * @returns {boolean} App installed boolean
 */
const checkMerchantActiveInstall = async (shop) => {
  if (!shop) throw new Error("shop not provided");
  else {
    const query = `
      query ($shop: String!) {
        checkMerchantActiveInstall(shop: $shop) 
      }
    `;

    const variables = {
      shop,
    };

    try {
      const response = await apolloFetch({
        query,
        variables,
      });

      return response.data.checkMerchantActiveInstall;
    } catch (error) {
      throw new Error(`Error checking merchant active install: ${error}`);
    }
  }
};

module.exports = {
  saveShopProperties,
  getShopAccessToken,
  sendCart,
  checkMerchantActiveInstall,
};
