require("isomorphic-fetch");
const { config } = require("dotenv");
const Koa = require("koa");
const next = require("next");
const { default: createShopifyAuth } = require("@shopify/koa-shopify-auth");
const { verifyRequest } = require("@shopify/koa-shopify-auth");
const { default: Shopify, ApiVersion } = require("@shopify/shopify-api");
const Router = require("koa-router");
const {
  getShopProperties,
  registerScriptTag,
  checkWebhookEndpoint,
} = require("./services/shopify-api");
const {
  saveShopProperties,
  getShopAccessToken,
  sendCart,
  checkMerchantActiveInstall,
} = require("./services/centoll-api");
const Sentry = require("@sentry/node");
const chalk = require("chalk");
const {
  registerWebhook,
  receiveWebhook,
  DeliveryMethod,
} = require("@shopify/koa-shopify-webhooks");

config();

/**
 * Shopify Context
 */
Shopify.Context.initialize({
  API_KEY: process.env.NEXT_PUBLIC_SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SHOPIFY_API_SCOPES.split(","),
  HOST_NAME: process.env.SHOPIFY_APP_URL.replace(/https:\/\//, ""),
  API_VERSION: ApiVersion.January21,
  IS_EMBEDDED_APP: true,
  SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
});

const webhookAddress =
  "arn:aws:events:eu-west-2::event-source/aws.partner/shopify.com/3905871/centoll-shopify";
const port = process.env.PORT || 3001;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

Sentry.init({
  dsn:
    "https://81536e3de7de4a938f8415a219cbcb85@o534327.ingest.sentry.io/5660612",
});

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.keys = [Shopify.Context.API_SECRET_KEY];

  server.use(
    /**
     * Shopify Authentication
     */
    createShopifyAuth({
      accessMode: "offline",
      async afterAuth(ctx) {
        const { shop, accessToken } = ctx.state.shopify;

        /**
         * Create ScriptTag for adding an iframe
         */
        try {
          await registerScriptTag(shop, accessToken);

          console.log(chalk.green(`Successfully registered ScriptTag!`));
        } catch (error) {
          throw new Error(`Error creating ScriptTag: ${error}`);
        }

        /**
         * Get shop properties and save them in the DB
         */
        try {
          const { shop: shop_result } = await getShopProperties(
            shop,
            accessToken
          );

          await saveShopProperties({
            id: shop_result.id,
            myshopifyDomain: shop_result.myshopify_domain,
            accessToken,
          });

          console.log(chalk.green(`Saved shop properties for ${shop}`));
        } catch (error) {
          throw new Error(`Error saving shop properties: ${error}`);
        }

        /**
         * Set shopOrigin cookie
         */
        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sameSite: "none",
        });

        /**
         * Register CARTS_UPDATE webhook
         *
         * Using Amazon SQS:
         * - address        : "arn:aws:lambda:eu-west-2:347805442942:function:cartUpdateWebhookShopify"
         * - deliveryMethod : DeliveryMethod.EventBridge
         *
         * Using local route:
         * - address        : `${HOST}/webhooks/carts/update`
         */
        const cart_update_webhook_registration = await registerWebhook({
          // address: `${process.env.SHOPIFY_APP_URL}/webhooks/carts/update`,
          address: webhookAddress,
          deliveryMethod: DeliveryMethod.EventBridge,
          topic: "CARTS_UPDATE",
          accessToken,
          shop,
          apiVersion: ApiVersion.January21,
        });

        if (cart_update_webhook_registration.success) {
          console.log(
            chalk.green(
              `Successfully registered CARTS_UPDATE webhook for domain ${shop}`
            )
          );
        } else {
          throw new Error(
            `Failed to register CARTS_UPDATE webhook for domain ${shop}: ${JSON.stringify(
              cart_update_webhook_registration.result
            )}`
          );
        }

        const returnUrl = `${process.env.SHOPIFY_APP_URL}?shop=${shop}`;

        ctx.redirect(returnUrl);
      },
    })
  );

  /**
   * GraphQL Middleware
   */
  router.post("/graphql", verifyRequest(), async (ctx, next) => {
    await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
  });

  /**
   * NextJS request handler
   */
  const handleRequest = async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  };

  /**
   * Check if app is installed an redirect or handle
   */
  router.get("/", async (ctx) => {
    const shop = ctx.query.shop;
    const app_is_installed = await checkMerchantActiveInstall(shop);
    // await checkWebhookEndpoint(shop, webhookAddress);

    console.log("app_is_installed", app_is_installed);

    if (!app_is_installed) {
      ctx.redirect(`/auth?shop=${shop}`);
    } else {
      await handleRequest(ctx);
    }
  });

  router.get("(/_next/static/.*)", handleRequest);
  router.get("/_next/webpack-hmr", handleRequest);
  router.get("(.*)", verifyRequest(), handleRequest);

  server.use(router.allowedMethods());
  server.use(router.routes());

  /**
   * Sentry capture.
   */
  server.on("error", (err, ctx) => {
    Sentry.withScope((scope) => {
      scope.addEventProcessor((event) => {
        return Sentry.Handlers.parseRequest(event, ctx.request);
      });

      Sentry.captureException(err);
    });
  });

  server.listen(port, () => {
    console.log(`🚀 Ready on http://localhost:${port}`);
  });
});
